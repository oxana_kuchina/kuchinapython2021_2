import random
from daun import Player

percents = {1: 100, 2: 90, 3: 80, 4: 70, 5: 60, 6: 50, 7: 40, 8: 30, 9: 20}

def chance(percent: int) -> bool:
    percent /= 10
    chance = random.randint(1, 9)
    return 0 < chance < percent

def valid_hit_power(power: int) -> int:
    return power if chance(percents.get(power)) else 0

def hit(player_one: Player, player_two: Player) -> bool:
    power = int(input("{} enter the power - ".format(player_one.username)))
    while not 0 < power < 10:
        power = int(input("wrong. Try again - ".format(player_one.username)))
    power = valid_hit_power(power)
    if power == 0:
        return False
    else:
        player_two -= power * 10
        return True

if __name__ == '__main__':
    print("the game starts")
    player_one = Player(input("enter first username - "))
    player_two = Player(input("enter second username - "))
    who_is_first = random.randint(1, 2)
    if who_is_first == 2:
        player_two, player_two = player_two, player_one
    flag = True
    while flag:
        if player_two.alive():
            if hit(player_one, player_two):
                print("{} you got it".format(player_one.username))
                print(player_two)
            else:
                print("{} you missed".format(player_one.username))
                print(player_two)
            player_one, player_two = player_two, player_one
        else:
            print("{} you won".format(player_one.username))
            flag = False