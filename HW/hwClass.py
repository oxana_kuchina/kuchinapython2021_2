class Player:
    def __init__(self, username: str = "player"):
        self.username = username
        self.hp = 150

    def __isub__(self, power: int = 0):
        self.hp -= power

    def __str__(self):
        return "{}: hp = {}".format(self.username, self.hp)

    def alive(self):
        return self.hp > 0