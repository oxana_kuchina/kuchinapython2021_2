def sort(lst: list[int]) -> list[int]:
    for i in range(len(lst)):
        for j in range(1, len(lst) - 1):
            if lst[j] < lst[j - 1]:
                elem = lst[j - 1]
                lst[j - 1] = lst[j]
                lst[j] = elem
    return lst


if __name__ == "__main__":
    lst = get_list()
    sort(int(input("введите число - ")))
    print(sort(lst))
    print(")")