if __name__ == '__main__' :
    num = int(input("Введите число от 0 до 100 - "))
    print("Число лежит в промежутке между 10 и 20")
    answer_one = (num > 10) and (num < 20)
    print(answer_one)
    print("Да" if answer_one else "Нет")
    print("Чётное ли оно?")
    answer_two = (num % 2) == 0
    print("Да" if answer_two else "Нет")
    print(num * 2)
    if num > 4:
        print("Yes")
    else:
        print("No")